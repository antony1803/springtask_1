package com.anton.demo.univer;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "students")
public class Student {

    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "first_name")
    private String surname;

    @Column(name = "last_name")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "university_group_id")
    private UniversityGroup group;

    //@MapsId
    @OneToOne
    @JoinColumn(name = "mark_id")
    private Mark_book gradeBook;

    @ManyToMany
    @JoinTable(name = "student_subject",
            joinColumns = @JoinColumn(name = "student_id"),
            inverseJoinColumns = @JoinColumn(name = "subject_id"))
    private List<Subject> subjects = new ArrayList<>();

    public Student(List<Student> students){}
    public  Student(){}

    public Student(String name, String surname, UniversityGroup group, Mark_book gradeBook, List<Subject> subjects) {
        this.name = name;
        this.surname = surname;
        this.group = group;
        this.gradeBook = gradeBook;
        this.subjects = subjects;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public UniversityGroup getGroup() {
        return group;
    }

    public void setGroup(UniversityGroup group) {
        this.group = group;
    }

    public Mark_book getGradeBook() {
        return gradeBook;
    }

    public void setGradeBook(Mark_book gradeBook) {
        this.gradeBook = gradeBook;
    }

    public List<Subject> getSubjects() {
        return this.subjects;
    }
    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }
}

