package com.anton.demo;


import com.anton.demo.repositories.MarkBookRepository;
import com.anton.demo.repositories.StudentRepository;
import com.anton.demo.repositories.SubjectRepository;
import com.anton.demo.repositories.UniversityGroupRepository;
import com.anton.demo.univer.Mark_book;
import com.anton.demo.univer.Student;
import com.anton.demo.univer.Subject;
import com.anton.demo.univer.UniversityGroup;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class StudentsApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudentsApplication.class, args);
    }

    @Bean
    public CommandLineRunner demo(StudentRepository studentRepository, MarkBookRepository markBookRepository, UniversityGroupRepository universityGroupRepository,
                                  SubjectRepository subjectRepository){
        return (args) -> {

            universityGroupRepository.save(new UniversityGroup(1));
            universityGroupRepository.save(new UniversityGroup(2));
            universityGroupRepository.save(new UniversityGroup(3));
            universityGroupRepository.save(new UniversityGroup(4));

            subjectRepository.save(new Subject("Math"));
            subjectRepository.save(new Subject("Physics"));
            subjectRepository.save(new Subject("English"));
            subjectRepository.save(new Subject("Russian"));
            subjectRepository.save(new Subject("Biology"));

            markBookRepository.save(new Mark_book(1523294));
            markBookRepository.save(new Mark_book(1726347));
            markBookRepository.save(new Mark_book(1643567));

            List<Subject> disciplineEntities = new ArrayList<>();
            disciplineEntities.add(subjectRepository.findByName("Math"));


            List<Student> studentsi = new ArrayList<>();



            disciplineEntities.add(subjectRepository.findByName("Physics"));
            studentRepository.save(new Student("Anton", "Kukyan", universityGroupRepository.findByGroup(1),
                    markBookRepository.findByGradebook(1523294), disciplineEntities));

            disciplineEntities.add(subjectRepository.findByName("Physics"));
            studentRepository.save(new Student("Nikita", "Titov", universityGroupRepository.findByGroup(2),
                    markBookRepository.findByGradebook(1643567), disciplineEntities));

            disciplineEntities.add(subjectRepository.findByName("Biology"));
            studentRepository.save(new Student("Evgeniy", "Skorohodov", universityGroupRepository.findByGroup(4),
                    markBookRepository.findByGradebook(1726347), disciplineEntities));

            studentsi.add(studentRepository.findByNameAndSurname("Anton","Kukyan"));

            if  (studentsi.size() > 0){
                subjectRepository.save(new Subject("ArtsAndCraft",studentsi));
            }



        };
    }
}
