package com.anton.demo.repositories;


import com.anton.demo.univer.Mark_book;
import org.springframework.data.repository.CrudRepository;

public interface MarkBookRepository extends CrudRepository<Mark_book, Integer> {
    Mark_book findByGradebook(Integer gradebook);
}
