package com.anton.demo.univer;

import javax.persistence.*;

@Entity
@Table(name = "mark_book")
public class Mark_book {

    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "mark_book_number")
    private Integer gradebook;

    @OneToOne(mappedBy = "gradeBook", cascade = CascadeType.ALL,orphanRemoval = true)
    private Student student;

    public Mark_book(){}

    public Mark_book(Integer gradebook) {
        this.gradebook = gradebook;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGradebook() {
        return gradebook;
    }

    public void setGradebook(Integer gradebook) {
        this.gradebook = gradebook;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student studentEntity) {
        this.student = student;
    }
}
