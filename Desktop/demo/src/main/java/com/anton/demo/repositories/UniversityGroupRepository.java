package com.anton.demo.repositories;


import com.anton.demo.univer.UniversityGroup;
import org.springframework.data.repository.CrudRepository;

public interface UniversityGroupRepository extends CrudRepository<UniversityGroup, Integer> {
    UniversityGroup findByGroup(Integer group);
}
